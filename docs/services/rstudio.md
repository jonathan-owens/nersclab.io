# RStudio

NERSC has retired its R Studio service.  If you are interested in instructions on how to run a private R studio instance,
please file a ticket.  However, we strongly encourage R users to try out Jupyter (see below).

!!! warning "R Studio users are encouraged to switch to Jupyter"
    NERSC strongly encourages users to use Jupyter ([jupyter.nersc.gov](https://jupyter.nersc.gov)) for their R
    analysis.  Jupyter offers better support and resources and
    it is easier for users to create custom environments with
    Anaconda.

    For details on Jupyter, see the [Jupyter Docs](../jupyter).

    For details on using Anaconda to create R environments, see the [R
    docs](../development/languages/r.md).
